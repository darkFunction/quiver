{
  "title": "Property wrappers",
  "cells": [
    {
      "type": "markdown",
      "data": "https://medium.com/better-programming/taking-swift-dependency-injection-to-the-next-level-b71114c6a9c6"
    },
    {
      "type": "markdown",
      "data": "```swift\n@propertyWrapper\nstruct ConsoleLogged<T> {\n\tprivate var value: T \n\t\t\n\tinit(wrappedValue: T) {\n\t\tself.value = wrappedValue\t\n\t}\n\t\n\tvar wrappedValue: T {\n\t\tget { value }\n\t\tmutating set {\n\t\t\tvalue = newValue\n\t\t\tprint(\"Log entry: \\(newValue)\")\n\t\t}\n\t}\t\n}\n```\n\nWithout property wrappers, to use the above wrapper we would be need to to manually get/set to the wrapped value inside a private variable like this:\n\n```swift\nstruct Foo {\n\tprivate var _bar = ConsoleLogged(0)\n\tvar bar: Int {\n\t\tget { _bar.wrappedValue }\n\t\tset { _bar.wrappedValue = newValue }\n\t}\n}\n```\n\n**With** property wrappers, syntax is as follows:\n\n```swift\nstruct Foo {\n\t@ConsoleLogged var bar = 0\n}\n```\n\n> There are two requirements for a property wrapper type:\n>   1. It must be defined with the attribute @propertyWrapper.\n>   2. It must have a wrappedValue property.\n\n# Access\n\nCan access the wrapper itself, and not the wrapped value, by prepending with an underscore (`_bar`). It has `private` protection so can't be accessed from outside the class.\n\nYou can return a **projectedValue**, if implemented, using the dollar sign. `projectedValue` can be any type. The protection level is not private. This might often be used just as a way to expose the private `_bar` wrapper type but is not limited to this. \n\n```swift\nstruct Foo {\n    @ConsoleLogged var bar = 0\n    \n    func test() {\n        print(bar)  // wrappedValue\n        print($bar) // projectedValue\n        print(_bar) // wrapper type itself\n    }\n}\n```\n\n# Init\n\nDifferent initialisers of the property wrapper are called depending on how it is initialised:\n\n```swift\n@ConsoleLogged var bar: Int   // calls init() \n@ConsoleLogged var bar = 5    // calls init(wrappedValue: 5) \n@ConsoleLogged(wrappedValue: 5, maxLines: 5) // calls init(wrappedValue:maxLines:)\n```"
    },
    {
      "type": "markdown",
      "data": "# Good ideas for property wrappers\n\n## Keychain\n\n```swift\n@Keychain(key: \"username\") var username: String?\n```\n\n... cool but you could just use \n\n```swift\n@Injected var keychain: KeychainManager\n```\n\nand minimise use of different property wrappers?"
    }
  ]
}